#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

void main()
{
    int APUESTA, ON, OFF, carta_1, carta_2, carta_3,carta_4, carta_cpu1, carta_cpu2, carta_cpu3, carta_cpu4, cartas_sumadas, cartas_sumadascpu, natural, naturalcpu; 
    //Damos las variables de las cartas, la apuesta, el encendido (ON) que sera para volver a jugar al finalizar el juego y cartas para repartir.  
    char name[6];
    puts("Bienvenido al juego de Bacara, ingrese su nickname: ");   // Saludamos al jugador y le solicitamos que ingrese su nickname de 8 letras maximo
    gets(name);
    
    srand(time(0)); 
    do
    {
        printf("Bien %s, realiza tu apuesta: \n $ ",name);  //Le solicitamos al jugador ingresar su apuesta 
        scanf("%d",&APUESTA);
       
        carta_1 = (rand()%13)+1;
    if (carta_1 < 10 && carta_1>1)
    {
        printf("La primera carta es un %d \n", carta_1);
    }
    else if (carta_1==1)
    {
        printf("La primera carta es un As \n");
    }
   else if (carta_1 == 10)
    {
        printf("La primera carta es un %d \n",carta_1);
        carta_1=0;
    }
    else if (carta_1 == 11)
    {
        printf("La primera carta es un Jota \n");
        carta_1=0;
    }
    else if (carta_1 == 12)
    {
        printf("La primera carta es una Reina \n");
        carta_1=0;
    }
    else
    {
        printf("La primera carta es un Rey \n");
        carta_1=0;
    }
      carta_2 = (rand()%13)+1;

    if (carta_2<10 && carta_2>1)
    {
        printf("La segunda carta es un %d \n", carta_2);
    }
    else if (carta_2==10)
    {
        printf("La segunda carta es un %d \n", carta_2);
        carta_2=0;
    }
    else if (carta_2==1)
    {
        printf("La segunda carta es un As \n");
    }
    else if (carta_2 == 11)
    {
        printf("La segunda carta es un Jota \n");
        carta_2=0;
    }
    else if (carta_2 == 12)
    {
        printf("La segunda carta es una Reina \n");
        carta_2=0;
    }
    else
    {
        printf("La segunda carta es un Rey \n");
        carta_2=0;
    }
      carta_cpu1 = (rand()%13)+1;

    if (carta_cpu1<10 && carta_cpu1>1)
    {
        printf("La primera carta de tu oponente es un %d \n", carta_cpu1);
    }
    else if (carta_cpu1==10)
    {
        printf("La primera carta de tu oponente es un %d \n", carta_cpu1);
        carta_cpu1=0;
    }
    else if (carta_cpu1==1)
    {
        printf("La primera carta de tu oponente es un As \n");
    }
    else if (carta_cpu1 == 11)
    {
        printf("La primera carta de tu oponente es un Jota \n");
        carta_cpu1=0;
    }
    else if (carta_cpu1 == 12)
    {
        printf("La primera carta de tu oponente es una Reina \n");
        carta_cpu1=0;
    }
    else
    {
        printf("La primera carta de tu oponente es un Rey \n");
        carta_cpu1=0;
    } 
      carta_cpu2 = (rand()%13)+1;

    if (carta_cpu2<10 && carta_cpu2>1)
    {
        printf("La segunda carta de tu oponente es un %d \n", carta_cpu2);
    }
      else if (carta_cpu2==10)
    {
        printf("La primera carta de tu oponente es un %d \n", carta_cpu2);
        carta_cpu2=0;
    }
    else if (carta_cpu2==1)
    {
        printf("La segunda carta de tu oponente es un As \n");
    }
    else if (carta_cpu2 == 11)
    {
        printf("La segunda carta de tu oponente es un Jota \n");
        carta_cpu2=0;
    }
    else if (carta_cpu2 == 12)
    {
        printf("La segunda carta de tu oponente es una Reina \n");
        carta_cpu2=0;
    }
    else
    {
        printf("La segunda carta de tu oponente es un Rey \n");
        carta_cpu2=0;
    }
            cartas_sumadas= carta_1 + carta_2;
            cartas_sumadascpu= carta_cpu1 + carta_cpu2;
             if(cartas_sumadas>=10)
              {
             cartas_sumadas=cartas_sumadas-10;
              }
             if(cartas_sumadascpu>=10)
              {
             cartas_sumadascpu=cartas_sumadascpu-10;
              }
            natural=cartas_sumadas;
            naturalcpu=cartas_sumadascpu;
      if((natural==8 || natural==9) || (naturalcpu==8 || naturalcpu==9))
        {
         if(natural==8 && naturalcpu!=9 && naturalcpu!=8)
        {
             printf("Vaya, has ganado por un NATURAL, Felicidades! \n");
        }
         else if (natural==9 && naturalcpu!=9)
         {
             printf("Vaya, has ganado por un NATURAL, Felicidades! \n");
         }
        else if ( natural==naturalcpu || cartas_sumadas==cartas_sumadascpu)
         {
             printf("Vaya, has empatado con tu oponente \n");
         }
        else if (naturalcpu==8 && natural!=8 && natural!=9)
         {
             printf("Vaya, tu oponente te ha derrotado por un NATURAL \n");
         }
        else if(naturalcpu==9 && natural!=9)
         {
             printf("Vaya, tu oponente te ha derrotado por un NATURAL \n");
         }
      }   
      else if((cartas_sumadas<6 || cartas_sumadascpu<6)||(cartas_sumadas<6 && cartas_sumadascpu<6))
      {     carta_3=0;
            carta_cpu3=0;
        if(cartas_sumadas<6)
      {    
         printf("vaya, tus 2 primeras cartas dieron como resultado de un 0 a un 5, toma otra carta: \n" );
          carta_3= rand()%10;
          if (carta_3<10 && carta_3>1)
         {
          printf("La tercera carta es un %d \n", carta_3);
         }
          else if(carta_3==10)
          {
              printf("La tercera carta es un %d \n", carta_3);
              carta_3==0;
          }
          else if (carta_3==1)
         {
          printf("La tercera carta es un As \n");
         }
          else if (carta_3 == 11)
         {
          printf("La tercera carta es un Jota \n");
         }
         else if (carta_3 == 12)
         {
          printf("La tercera carta es una Reina \n");
         }
         else
         {
          printf("La tercera carta es un Rey \n");
         }
       }
      if(cartas_sumadascpu<6)
        {
        printf("vaya, las 2 primeras cartas de tu rival dieron como resultado de un 0 a un 5, se le entregara otra carta: \n");
        carta_cpu3= rand()%10;
        
        if (carta_cpu3<10 && carta_cpu3>1)
         {
          printf("La tercera carta de tu oponente es un %d \n", carta_cpu3);
         }
        else if (carta_cpu3==10)
    {
        printf("La tercera carta de tu oponente es un %d \n", carta_cpu3);
        carta_cpu3=0;
    }
          else if (carta_cpu3==1)
         {
          printf("La tercera carta de tu oponente es un As \n");
         }
          else if (carta_cpu3 == 11)
         {
          printf("La tercera carta de tu oponente es un Jota \n");
         }
         else if (carta_cpu3 == 12)
         {
          printf("La tercera carta de tu oponente es una Reina \n");
         }
         else
         {
          printf("La tercera carta de tu oponente es un Rey \n");
         }
        }
            cartas_sumadas= cartas_sumadas+carta_3;
            cartas_sumadascpu= cartas_sumadascpu+carta_cpu3;
            
             if(cartas_sumadas>=10)
              {
             cartas_sumadas=cartas_sumadas-10;
              }
             if(cartas_sumadascpu>=10)
              {
             cartas_sumadascpu=cartas_sumadascpu-10;
              }
         printf("La suma de tus cartas a resultado %d \n",cartas_sumadas);
         
         printf("Las suma de las cartas de tu oponente es %d \n ",cartas_sumadascpu);
         
         if(cartas_sumadas>cartas_sumadascpu)
         {
             printf("Has ganado, felicidades! \n");
         }
         else if(cartas_sumadas<cartas_sumadascpu)
         {
             printf("Lastima, la banca a ganado \n");
         }
         else 
         {
             printf("Vaya, han empatado \n");  
         }
      }
      else 
      {
         printf("La suma de tus cartas a resultado %d \n",cartas_sumadas);
         printf("Las suma de las cartas de tu oponente es %d \n ",cartas_sumadascpu);
         if(cartas_sumadas>cartas_sumadascpu)
         {
             printf("Has ganado, felicidades! \n");
         }
         else if(cartas_sumadas<cartas_sumadascpu)
         {
             printf("Lastima, la banca a ganado \n");
         }
         else 
         {
             printf("Vaya, han empatado \n");  
         }
        }
         printf("\n Si deseas seguir jugando %s digita 1, de otra forma digita cualquier otro numero: \n",name);
         scanf("%d",&ON); 
    } 
    while(ON<=1);
    printf(" Game Over \n ");
}    